require 'net/http'
require 'uri'

def all_incoming_calls_since(param)
  uri = URI.parse('https://api.binotel.com/api/2.0/stats/all-incoming-calls-since.json')
  post_binotel(uri, time_one_params(param))
end

def all_outgoing_calls_since(param)
  uri = URI.parse('https://api.binotel.com/api/2.0/stats/all-outgoing-calls-since.json')
  post_binotel(uri, time_one_params(param))
end

def all_calls_since(param)
  all_incoming_calls_since(param).merge(all_outgoing_calls_since(param))
end

def call_record(param)
  uri = URI.parse('https://api.binotel.com/api/2.0/stats/call-record.json')
  time = Time.now.to_i
  param2 = { timestamp: time, callID: param.to_s }.to_json
  params =  {"timestamp": time, "callID": param.to_s, "signature": signature_binotel(param2), "key": key_binotel }.to_json
  post_binotel(uri, params)
end

def calltracking_calls_for_period(start_p, stop_p)
  start = (Time.now-(start_p.to_i*86400)).to_i
  stop = (Time.now-(stop_p.to_i*86400)).to_i
  param2 = { startTime: start, stopTime: stop }.to_json
  params =  {startTime: start, stopTime: stop, "signature": signature_binotel(param2), "key": key_binotel }.to_json
  uri = URI.parse('https://api.binotel.com/api/2.0/stats/calltracking-calls-for-period.json')
  post_binotel(uri, params)
end

def time_one_params(param)
  time = (Time.now-param.to_i).to_i
  param2 = { timestamp: time }.to_json
  params =  {"timestamp": time, "signature": signature_binotel(param2), "key": key_binotel }.to_json
end

def post_binotel(uri, params)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  http.start {
    request = Net::HTTP::Post.new(
        uri.request_uri,
        'Content-Type' => 'application/json'
    )
    request.body = params

    response = http.request(request)
    body = JSON.parse(response.body)
  }
end