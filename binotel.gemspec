$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "binotel/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "binotel"
  s.version     = Binotel::VERSION
  s.authors     = ["Eugene Peluhnya"]
  s.email       = ["peluhnya@outlook.com"]
  s.homepage    = "http://iteasycode.com"
  s.summary     = "Binotel API."
  s.description = "Binotel API access."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails"
  s.add_development_dependency "sqlite3"
end
